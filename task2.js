const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

// In **task2.js**, you will need to modify the existing functionality provided.

// The script is supposed to:

// - Generate 10 random 3 letter acronyms, e.g. USA, AUD, ABC.
// - For each acronym, load a list of definitions from <http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=XYZ> (where `XYZ` is the generated acronym).
// - Output a json file **output3.json** with the array property `definitions`, which contains each downloaded JSON objects.

// However, if you run the script, you will notice that the created JSON file always contains an empty `definitions` property, even though the console reports downloading the acronym data.

// Fix the code to match the listed functionality.

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {};

const getAcronym = function () {
  var acronym = randomstring.generate(3).toUpperCase();
  got(inputURL + acronym)
    .then((response) => {
      console.log("got data for acronym", acronym);
      console.log("add returned data to definitions array");
      output.definitions.push(response.body);
      if (output.definitions.length < 10) {
        console.log("calling looping function again");
        getAcronym();
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

const getAcronymFixed = async () => {
  let acronym = randomstring.generate(3).toUpperCase();
  const response = await got(inputURL + acronym);
  if (response.body.includes("[]")) {
    console.log("no data found for acronym");
    return;
  }
  console.log("got data for acronym", acronym);
  console.log("add returned data to definitions array");
  console.log(response.body);
  output.definitions.push(response.body);
};

const getAcronyms = async () => {
  while (output.definitions.length < 10) {
    let _data = await getAcronymFixed();
  }
  console.log("saving output file formatted with 2 space indenting");
  jsonfile.writeFile(outputFile, output, { spaces: 2 }, function (err) {
    console.log("All done!");
  });
};

console.log("getting 10 acronyms");
output.definitions = [];
console.log("creating looping function");
getAcronyms();

// console.log("calling looping function");
// getAcronym();
