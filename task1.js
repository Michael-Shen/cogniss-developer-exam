// ## Task 1

// In **task1.js**, you will need to create the entire functionality yourself.

// Inside the script, load the `names` array from the JSON file **input2.json**.

//For each name, reverse it, append 5 random characters plus "@gmail.com". The end result should be an array of fake email addresses.

// Save the array as the property `emails` into a JSON file called **output2.json**.
const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");
const { default: create } = require("got/dist/source/create");

const inputFile = "input2.json";
const outputFile = "output2.json";

const createGmail = (name) => {
  const reversedName = name.split("").reverse().join("");
  const randomString = randomstring.generate(5);
  const emailSuffix = "gmail.com";
  const email = `${reversedName}${randomString}@${emailSuffix}`;
  return email;
};

var output = {};
jsonfile.readFile(inputFile, function (err, body) {
  let names = body.names;
  output.emails = [];
  names.forEach((name) => {
    output.emails.push(createGmail(name));
  });
  jsonfile.writeFile(outputFile, output, { spaces: 2 }, function (err) {
    console.log("All done!");
  });
});
